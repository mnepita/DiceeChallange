function rollDices() {
    //Stores the six dicee faces images sources as an array for later access 
    var images = ["images/dice1.png", "images/dice2.png", "images/dice3.png", "images/dice4.png", "images/dice5.png", "images/dice6.png"];

    // Generates random numbers between 1 and 6
    var randomNumber1 = Math.floor(Math.random() * 6) + 1;
    var randomNumber2 = Math.floor(Math.random() * 6) + 1;

    //Stores the randomly generated number in a variable
    var newImage1 = images[randomNumber1 - 1];
    var newImage2 = images[randomNumber2 - 1];

    //assigns the dice Face for player 1 to the new generated image src
    var diceFace1 = document.querySelector(".img1");
    diceFace1.src = newImage1;

    var diceFace2 = document.querySelector(".img2");
    diceFace2.src = newImage2;

    // Determines which player is the winner
    if (randomNumber1 > randomNumber2) {
        document.querySelector("h1").innerHTML = "🚩Player 1 Wins";
    } else if (randomNumber1 === randomNumber2) {
        document.querySelector("h1").innerHTML = "🚩It's a draw!!🚩";
    } else
        document.querySelector("h1").innerHTML = "🚩Player 2 Wins";
}

// Running the app
rollDices();
